//
//  Coordinator.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation
import UIKit

class Coordinator {

    private let assembly: Assembly

    private var navigationViewController: UINavigationController?

    init(assembly: Assembly) {
        self.assembly = assembly
    }

    func start(window: UIWindow) {
        let adminView = assembly.makeAdmin(output: self)
        navigationViewController = UINavigationController(rootViewController: adminView)
        window.rootViewController = navigationViewController
        window.makeKeyAndVisible()
    }
}

// MARK: - AdminOutput

extension Coordinator: AdminOutput {
    func showListStudents() {
        let listStudentsView = assembly.makeListStudents(output: self)
        navigationViewController?.pushViewController(listStudentsView, animated: true)
    }

    func showListLessons() {
        let listLessonsView = assembly.makeListLessons(output: self)
        navigationViewController?.pushViewController(listLessonsView, animated: true)
    }

    func showSchedule() {
        let scheduleView = assembly.makeSchedule(output: self)
        navigationViewController?.pushViewController(scheduleView, animated: true)
    }
}

// MARK: - ListStudentsOutput

extension Coordinator: ListStudentsOutput {
    func showListEvaluationOfStudent(_ student: Student) {
        let studentEvaluationsView = assembly.makeStudentEvaluations(output: self, student: student)
        navigationViewController?.pushViewController(studentEvaluationsView, animated: true)
    }
}

// MARK: - ListLessonsOutput

extension Coordinator: ListLessonsOutput {
    func showListEvaluation(_ lesson: Lesson) {
        let studentsEvaluationsView = assembly.makeStudentsEvaluations(output: self, lesson: lesson)
        navigationViewController?.pushViewController(studentsEvaluationsView, animated: true)
    }
}

// MARK: - StudentsEvaluationsOutput

extension Coordinator: StudentsEvaluationsOutput { }

// MARK: - StudentEvaluationsOutput

extension Coordinator: StudentEvaluationsOutput { }

// MARK: - ScheduleOutput

extension Coordinator: ScheduleOutput { }
