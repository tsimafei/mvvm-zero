//
//  ListStudentsViewModel.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation

// MARK: - ListStudentsViewModelInterface

protocol ListStudentsViewModelInterface {

    var tableData: [(section: String, rows: [StudentTableViewCellModel])] { get }

    func viewDidLoad()

    func rowTapped(_ section: Int, row: Int)
}

// MARK: - ListStudentsViewModel

class ListStudentsViewModel {

    weak var view: ListStudentsViewControllerInterface?
    private weak var output: ListStudentsOutput?
    private let useCase: StudentsUseCase

    var tableData = [(section: String, rows: [StudentTableViewCellModel])]()

    private var students = [Student]()

    init(output: ListStudentsOutput, useCase: StudentsUseCase) {
        self.output = output
        self.useCase = useCase
    }
}

// MARK: - ListStudentsViewModelInterface

extension ListStudentsViewModel: ListStudentsViewModelInterface {

    func viewDidLoad() {
        students = useCase.getAllStudents()
        tableData = Dictionary(grouping: students) {
            $0.lastName.first ?? "*"
        }.compactMap {
            (section: String($0.key), rows: $0.value.compactMap { student in
                let diff = Calendar.current.dateComponents([.year], from: student.startDate, to: Date()).day ?? 0
                return StudentTableViewCellModel(
                    id: student.id,
                    title: "\(student.lastName) \(student.name)",
                    subTitle: "Курс: \(diff + 1)"
                )
            })
        }

        tableData = tableData.sorted { $0.section < $1.section }
    }

    func rowTapped(_ section: Int, row: Int) {
        guard let student = students.first(where: { $0.id == tableData[section].rows[row].id }) else { return }
        output?.showListEvaluationOfStudent(student)
    }
}
