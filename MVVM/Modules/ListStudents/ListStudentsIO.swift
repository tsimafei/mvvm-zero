//
//  ListStudentsIO.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation

// MARK: - ListStudentsOutput

protocol ListStudentsOutput: AnyObject {
    func showListEvaluationOfStudent(_ student: Student)
}

// MARK: - ListStudentsInput

protocol ListStudentsInput: AnyObject { }
