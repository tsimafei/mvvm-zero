//
//  ListStudentsViewController.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation
import UIKit

private enum Constants {
    static var title: String { "Список студентов" }

    static var studentTableViewCellIdentifier: String { "StudentTableViewCell" }

    static var backgroundColor: UIColor { .white }
}

// MARK: - ListStudentsViewControllerInterface

protocol ListStudentsViewControllerInterface: AnyObject {
}

// MARK: - ListStudentsViewController

class ListStudentsViewController: UIViewController {

    private let viewModel: ListStudentsViewModelInterface

    let tableView = UITableView()

    init(viewModel: ListStudentsViewModelInterface) {
        self.viewModel = viewModel

        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.viewDidLoad()
        setupTable()

        setupContstraints()
        setupUI()
    }

    private func setupUI() {
        view.backgroundColor = Constants.backgroundColor
        title = Constants.title
    }

    private func setupContstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate(
            [
                tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                tableView.topAnchor.constraint(equalTo: view.topAnchor),
                tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ]
        )
    }

    private func setupTable() {
        view.addSubview(tableView)

        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(StudentTableViewCell.self, forCellReuseIdentifier: Constants.studentTableViewCellIdentifier)
    }
}

// MARK: - ListStudentsViewControllerInterface

extension ListStudentsViewController: ListStudentsViewControllerInterface {
}

// MARK: - UITableViewDataSource, UITableViewDelegate

extension ListStudentsViewController: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        viewModel.tableData.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.tableData[section].rows.count
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        viewModel.tableData[section].section
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(
                withIdentifier: Constants.studentTableViewCellIdentifier,
                for: indexPath
            ) as? StudentTableViewCell
        else { return UITableViewCell() }

        cell.setup(with: viewModel.tableData[indexPath.section].rows[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.rowTapped(indexPath.section, row: indexPath.row)
    }
}
