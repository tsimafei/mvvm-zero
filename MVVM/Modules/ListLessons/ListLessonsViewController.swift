//
//  ListLessonsViewController.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation
import UIKit

private enum Constants {
    static var title: String { "Список уроков" }

    static var lessonsTableViewCellIdentifier: String { "LessonsTableViewCell" }

    static var backgroundColor: UIColor { .white }
}

// MARK: - ListLessonsViewControllerInterface

protocol ListLessonsViewControllerInterface: AnyObject {
}

// MARK: - ListLessonsViewController

class ListLessonsViewController: UIViewController {

    private let viewModel: ListLessonsViewModelInterface

    let tableView = UITableView()

    init(viewModel: ListLessonsViewModelInterface) {
        self.viewModel = viewModel

        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.viewDidLoad()
        setupTable()

        setupContstraints()
        setupUI()
    }

    private func setupUI() {
        view.backgroundColor = Constants.backgroundColor
        title = Constants.title
    }

    private func setupContstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate(
            [
                tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                tableView.topAnchor.constraint(equalTo: view.topAnchor),
                tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ]
        )
    }

    private func setupTable() {
        view.addSubview(tableView)

        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(LessonTableViewCell.self, forCellReuseIdentifier: Constants.lessonsTableViewCellIdentifier)
    }
}

// MARK: - ListLessonsViewControllerInterface

extension ListLessonsViewController: ListLessonsViewControllerInterface {
}

// MARK: - UITableViewDataSource, UITableViewDelegate

extension ListLessonsViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.tableData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(
                withIdentifier: Constants.lessonsTableViewCellIdentifier,
                for: indexPath
            ) as? LessonTableViewCell
        else { return UITableViewCell() }

        cell.setup(with: viewModel.tableData[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.rowTapped(indexPath.row)
    }
}
