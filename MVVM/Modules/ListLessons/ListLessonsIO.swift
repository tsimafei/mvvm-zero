//
//  ListLessonsIO.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation

// MARK: - ListLessonsOutput

protocol ListLessonsOutput: AnyObject {
    func showListEvaluation(_ lesson: Lesson)
}

// MARK: - ListLessonsInput

protocol ListLessonsInput: AnyObject { }
