//
//  StudentTableViewCell.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation
import UIKit

private enum Constants {
    static var leftRightSpacing: CGFloat { 16.0 }
    static var topBottomSpacing: CGFloat { 16.0 }

    static var titleFont: UIFont { .boldSystemFont(ofSize: 16) }
}

// MARK: - LessonTableViewCellModel

struct LessonTableViewCellModel {
    let title: String
}

// MARK: - LessonTableViewCell

class LessonTableViewCell: UITableViewCell {

    private let titleLabel = UILabel()

    var model: LessonTableViewCellModel?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
        setupContstraints()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupUI() {
        addSubview(titleLabel)

        titleLabel.font = Constants.titleFont
    }

    private func setupContstraints() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate(
            [
                titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.leftRightSpacing),
                titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.leftRightSpacing),
                titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: Constants.topBottomSpacing),
                titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Constants.topBottomSpacing)
            ]
        )
    }

    func setup(with model: LessonTableViewCellModel) {
        self.model = model

        titleLabel.text = model.title
    }
}
