//
//  ListLessonsViewModel.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation

// MARK: - ListLessonsViewModelInterface

protocol ListLessonsViewModelInterface {

    var tableData: [LessonTableViewCellModel] { get }

    func viewDidLoad()

    func rowTapped(_ row: Int)
}

// MARK: - ListLessonsViewModel

class ListLessonsViewModel {

    weak var view: ListLessonsViewControllerInterface?
    private weak var output: ListLessonsOutput?

    private let useCase: StudentsUseCase

    private var lessons: [Lesson] = []

    var tableData = [LessonTableViewCellModel]()

    init(output: ListLessonsOutput, useCase: StudentsUseCase) {
        self.output = output
        self.useCase = useCase
    }
}

// MARK: - ListLessonsViewModelInterface

extension ListLessonsViewModel: ListLessonsViewModelInterface {

    func viewDidLoad() {
        lessons = useCase.getAllLessons()
        tableData = lessons.compactMap { LessonTableViewCellModel(title: $0.title) }
    }

    func rowTapped(_ row: Int) {
        output?.showListEvaluation(lessons[row])
    }
}
