//
//  ScheduleIO.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation

// MARK: - ScheduleOutput

protocol ScheduleOutput: AnyObject { }

// MARK: - ScheduleInput

protocol ScheduleInput: AnyObject { }
