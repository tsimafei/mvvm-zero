//
//  ScheduleViewController.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation
import UIKit

private enum Constants {
    static var title: String { "Расписание" }

    static var calendarCollectionViewCellIdentifier: String { "CalendarCollectionViewCell" }

    static var backgroundColor: UIColor { .white }
}

// MARK: - ScheduleViewControllerInterface

protocol ScheduleViewControllerInterface: AnyObject { }

// MARK: - ScheduleViewController

class ScheduleViewController: UIViewController {

    private let viewModel: ScheduleViewModelInterface

    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewLayout())

    init(viewModel: ScheduleViewModelInterface) {
        self.viewModel = viewModel

        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.viewDidLoad()
        setupCollectionView()

        setupContstraints()
        setupUI()
    }

    private func setupUI() {
        view.backgroundColor = Constants.backgroundColor
        self.title = Constants.title
    }

    private func setupContstraints() {
        collectionView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate(
            [
                collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                collectionView.topAnchor.constraint(equalTo: view.topAnchor),
                collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ]
        )
    }

    private func setupCollectionView() {
        setupLayout()

        view.addSubview(collectionView)

        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(
            CalendarCollectionViewCell.self,
            forCellWithReuseIdentifier: Constants.calendarCollectionViewCellIdentifier
        )

        collectionView.backgroundColor = .lightGray
    }

    private func setupLayout() {
        let itemSize = NSCollectionLayoutSize(
            widthDimension: .absolute(100),
            heightDimension: .absolute(50)
        )

        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = .zero
        item.edgeSpacing = .init(leading: nil, top: nil, trailing: .fixed(1), bottom: .fixed(1))

        let groupSize = NSCollectionLayoutSize(
            widthDimension: .absolute(CGFloat(8 * itemSize.widthDimension.dimension + 8)),
            heightDimension: .absolute(CGFloat(itemSize.heightDimension.dimension + 1))
        )

        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize: groupSize,
            subitems: (0...8).compactMap { _ in item }
        )
        group.contentInsets = .zero
        group.edgeSpacing = nil
        group.interItemSpacing = .none

        let section = NSCollectionLayoutSection(group: group)
        let layout = UICollectionViewCompositionalLayout(section: section)

        collectionView.setCollectionViewLayout(
            layout,
            animated: false,
            completion: nil
        )
    }
}

// MARK: - ScheduleViewControllerInterface

extension ScheduleViewController: ScheduleViewControllerInterface { }

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource

extension ScheduleViewController: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.collectionData.count
    }

    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        guard
            let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: Constants.calendarCollectionViewCellIdentifier,
                for: indexPath
            ) as? CalendarCollectionViewCell
        else { return UICollectionViewCell() }

        cell.setup(with: viewModel.collectionData[indexPath.row])
        return cell
    }
}
