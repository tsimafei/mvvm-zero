//
//  LessonCollectionViewCell.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation
import UIKit

private enum Constants {
    static var leftRightSpacing: CGFloat { 16.0 }
    static var topBottomSpacing: CGFloat { 16.0 }

    static var titleFont: UIFont { .boldSystemFont(ofSize: 16) }
}

// MARK: - CalendarCollectionViewCellModel

struct CalendarCollectionViewCellModel {
    let title: String
}

// MARK: - CalendarCollectionViewCell

class CalendarCollectionViewCell: UICollectionViewCell {

    private let titleLabel = UILabel()

    private var model: CalendarCollectionViewCellModel?

    // MARK: - Constructor

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }

    private func setupUI() {
        addSubview(titleLabel)

        titleLabel.font = Constants.titleFont
        titleLabel.textAlignment = .center
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.numberOfLines = 2
        setupContstraints()

        backgroundColor = .white
    }

    private func setupContstraints() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate(
            [
                titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.leftRightSpacing),
                titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.leftRightSpacing),
                titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: Constants.topBottomSpacing),
                titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Constants.topBottomSpacing)
            ]
        )
    }

    func setup(with model: CalendarCollectionViewCellModel) {
        self.model = model

        titleLabel.text = model.title
    }
}
