//
//  ScheduleViewModel.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation

// MARK: - ScheduleViewModelInterface

protocol ScheduleViewModelInterface {

    var collectionData: [CalendarCollectionViewCellModel] { get }

    func viewDidLoad()
}

// MARK: - ScheduleViewModel

class ScheduleViewModel {

    weak var view: ScheduleViewControllerInterface?
    private weak var output: ScheduleOutput?

    private let useCase: StudentsUseCase

    var collectionData = [CalendarCollectionViewCellModel]()

    init(output: ScheduleOutput, useCase: StudentsUseCase) {
        self.output = output
        self.useCase = useCase
    }
}

// MARK: - ScheduleViewModelInterface

extension ScheduleViewModel: ScheduleViewModelInterface {

    func viewDidLoad() {
        collectionData.append(CalendarCollectionViewCellModel(title: ""))
        DateFormatter().weekdaySymbols.forEach {
            collectionData.append(CalendarCollectionViewCellModel(title: $0))
        }

        let schedule = useCase.getSchedule()

        let currentCalendar = Calendar.current

        let numberFormatter = NumberFormatter()
        numberFormatter.minimumIntegerDigits = 2
        numberFormatter.maximumIntegerDigits = 2

        (0...16).forEach { time in
            let time = time * 30
            let hourNumber = time / 60 + 8
            let timeNumber = time % 60

            let hoursString = numberFormatter.string(from: NSNumber(value: hourNumber)) ?? ""
            let timeString = numberFormatter.string(from: NSNumber(value: timeNumber)) ?? ""

            collectionData.append(CalendarCollectionViewCellModel(title: "\(hoursString):\(timeString)"))
            (1...7).forEach { dateNumber in
                if let lesson = schedule.first(where: { lesson in
                    lesson.dates.contains(where: { date in
                        let weekday = currentCalendar.component(.weekday, from: date)
                        let hourDate = currentCalendar.component(.hour, from: date)
                        let minute = currentCalendar.component(.minute, from: date)
                        print(
                            """
                            \(lesson.lesson.title) \(weekday) \(hourDate) \(minute) -
                            """
                        )
                        return weekday == dateNumber && hourDate == hourNumber && minute == timeNumber
                    })
                }) {
                    collectionData.append(CalendarCollectionViewCellModel(title: lesson.lesson.title))
                } else {
                    collectionData.append(CalendarCollectionViewCellModel(title: ""))
                }
            }
        }
    }
}
