//
//  AdminViewController.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation
import UIKit

private enum Constants {
    static var stackViewSpacing: CGFloat { 16 }
    static var stackViewWidth: CGFloat { 200 }

    static var listButtonTitle: String { "Список студентов" }
    static var evaluationsButtonTitle: String { "Оценки студентов" }
    static var lessonsButtonTitle: String { "Расписание" }

    static var buttonCornerRadius: CGFloat { 8.0 }
    static var buttonTextColor: UIColor { .black }
    static var backgroundButtonColor: UIColor { UIColor.lightGray.withAlphaComponent(0.3) }

    static var backgroundColor: UIColor { .white }
}

// MARK: - AdminViewControllerInterface

protocol AdminViewControllerInterface: AnyObject {
}

// MARK: - AdminViewController

class AdminViewController: UIViewController {

    private let viewModel: AdminViewModelInterface

    private let stackView = UIStackView()

    private let studentsListButton = UIButton()
    private let studentsEvaluationsButton = UIButton()
    private let studentsLessonsButton = UIButton()

    init(viewModel: AdminViewModelInterface) {
        self.viewModel = viewModel

        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupStackView()

        setupButton(studentsListButton, title: Constants.listButtonTitle, action: #selector(listButtonTapped))
        setupButton(
            studentsEvaluationsButton,
            title: Constants.evaluationsButtonTitle,
            action: #selector(evaluationsButtonTapped)
        )
        setupButton(studentsLessonsButton, title: Constants.lessonsButtonTitle, action: #selector(lessonsButtonTapped))

        setupContstraints()

        setupUI()
    }

    private func setupUI() {
        view.backgroundColor = Constants.backgroundColor
    }

    private func setupButton(_ button: UIButton, title: String, action: Selector) {
        view.addSubview(button)
        button.setTitle(title, for: .normal)
        button.addTarget(self, action: action, for: .touchUpInside)
        button.setTitleColor(Constants.buttonTextColor, for: .normal)
        button.backgroundColor = Constants.backgroundButtonColor
        button.layer.cornerRadius = Constants.buttonCornerRadius
        stackView.addArrangedSubview(button)
    }

    private func setupStackView() {
        view.addSubview(stackView)
        stackView.axis = .vertical
        stackView.spacing = Constants.stackViewSpacing
    }

    private func setupContstraints() {
        stackView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate(
            [
                stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                stackView.widthAnchor.constraint(equalToConstant: Constants.stackViewWidth)
            ]
        )
    }

    // MARK: - Actions

    @objc
    private func listButtonTapped() {
        viewModel.listButtonTapped()
    }

    @objc
    private func evaluationsButtonTapped() {
        viewModel.evaluationsButtonTapped()
    }

    @objc
    private func lessonsButtonTapped() {
        viewModel.lessonsButtonTapped()
    }
}

// MARK: - AdminViewControllerInterface

extension AdminViewController: AdminViewControllerInterface {
}
