//
//  AdminIO.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation

// MARK: - AdminOutput

protocol AdminOutput: AnyObject {
    func showListStudents()
    func showListLessons()
    func showSchedule()
}

// MARK: - AdminInput

protocol AdminInput: AnyObject { }
