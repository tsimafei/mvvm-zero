//
//  AdminViewModel.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation

// MARK: - AdminViewModelInterface

protocol AdminViewModelInterface {
    func listButtonTapped()
    func evaluationsButtonTapped()
    func lessonsButtonTapped()
}

// MARK: - AdminViewModel

class AdminViewModel {

    weak var view: AdminViewControllerInterface?
    private weak var output: AdminOutput?

    init(output: AdminOutput) {
        self.output = output
    }
}

// MARK: - AdminViewModelInterface

extension AdminViewModel: AdminViewModelInterface {
    func listButtonTapped() {
        output?.showListStudents()
    }

    func evaluationsButtonTapped() {
        output?.showListLessons()
    }

    func lessonsButtonTapped() {
        output?.showSchedule()
    }
}
