//
//  Assembly.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation
import UIKit

class Assembly {

    lazy var studentsAssembly: StudentsUseCase = {
        StudentsUseCaseImpl()
    }()

    func makeAdmin(output: AdminOutput) -> UIViewController {
        let viewModel = AdminViewModel(output: output)
        let view = AdminViewController(viewModel: viewModel)
        viewModel.view = view

        return view
    }

    func makeListStudents(output: ListStudentsOutput) -> UIViewController {
        let viewModel = ListStudentsViewModel(output: output, useCase: studentsAssembly)
        let view = ListStudentsViewController(viewModel: viewModel)
        viewModel.view = view

        return view
    }

    func makeListLessons(output: ListLessonsOutput) -> UIViewController {
        let viewModel = ListLessonsViewModel(output: output, useCase: studentsAssembly)
        let view = ListLessonsViewController(viewModel: viewModel)
        viewModel.view = view

        return view
    }

    func makeStudentsEvaluations(output: StudentsEvaluationsOutput, lesson: Lesson) -> UIViewController {
        let viewModel = StudentsEvaluationsViewModel(output: output, useCase: studentsAssembly, lesson: lesson)
        let view = StudentsEvaluationsViewController(viewModel: viewModel)
        viewModel.view = view

        return view
    }

    func makeStudentEvaluations(output: StudentEvaluationsOutput, student: Student) -> UIViewController {
        let viewModel = StudentEvaluationsViewModel(output: output, student: student)
        let view = StudentEvaluationsViewController(viewModel: viewModel)
        viewModel.view = view

        return view
    }

    func makeSchedule(output: ScheduleOutput) -> UIViewController {
        let viewModel = ScheduleViewModel(output: output, useCase: studentsAssembly)
        let view = ScheduleViewController(viewModel: viewModel)
        viewModel.view = view

        return view
    }
}
