//
//  StudentsEvaluationsViewModel.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation

// MARK: - StudentsEvaluationsViewModelInterface

protocol StudentsEvaluationsViewModelInterface {

    var tableData: [EvaluationTableViewCellModel] { get }

    func viewDidLoad()
}

// MARK: - StudentsEvaluationsViewModel

class StudentsEvaluationsViewModel {

    weak var view: StudentsEvaluationsViewControllerInterface?
    private weak var output: StudentsEvaluationsOutput?

    private let useCase: StudentsUseCase
    private let lesson: Lesson

    var tableData = [EvaluationTableViewCellModel]()

    init(output: StudentsEvaluationsOutput, useCase: StudentsUseCase, lesson: Lesson) {
        self.output = output
        self.useCase = useCase
        self.lesson = lesson
    }
}

// MARK: - StudentsEvaluationsViewModelInterface

extension StudentsEvaluationsViewModel: StudentsEvaluationsViewModelInterface {

    func viewDidLoad() {
        tableData = useCase.getAllStudents().compactMap { student in
            guard let evaluation = student.evaluations.first(where: { $0.lesson.id == lesson.id }) else { return nil }
            return EvaluationTableViewCellModel(
                title: "\(student.lastName) \(student.name)",
                subTitle: "\(round(evaluation.mark))"
            )
        }
        view?.setupTitle("Оценки по \(lesson.title)")
    }
}
