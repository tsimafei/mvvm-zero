//
//  StudentTableViewCell.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation
import UIKit

private enum Constants {
    static var leftRightSpacing: CGFloat { 16.0 }
    static var topBottomSpacing: CGFloat { 16.0 }

    static var titleFont: UIFont { .boldSystemFont(ofSize: 16) }
    static var subTitleFont: UIFont { .systemFont(ofSize: 14) }
}

// MARK: - EvaluationTableViewCellModel

struct EvaluationTableViewCellModel {
    let title: String
    let subTitle: String
}

// MARK: - EvaluationTableViewCell

class EvaluationTableViewCell: UITableViewCell {

    private let stackView = UIStackView()

    private let titleLabel = UILabel()
    private let subTitleLabel = UILabel()

    var model: EvaluationTableViewCellModel?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
        setupContstraints()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupUI() {
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(subTitleLabel)
        addSubview(stackView)

        stackView.axis = .horizontal

        titleLabel.font = Constants.titleFont
        subTitleLabel.font = Constants.subTitleFont

        subTitleLabel.textAlignment = .right
    }

    private func setupContstraints() {
        stackView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate(
            [
                stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.leftRightSpacing),
                stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.leftRightSpacing),
                stackView.topAnchor.constraint(equalTo: topAnchor, constant: Constants.topBottomSpacing),
                stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Constants.topBottomSpacing)
            ]
        )
    }

    func setup(with model: EvaluationTableViewCellModel) {
        self.model = model

        titleLabel.text = model.title
        subTitleLabel.text = model.subTitle
    }
}
