//
//  StudentsEvaluationsIO.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation

// MARK: - StudentsEvaluationsOutput

protocol StudentsEvaluationsOutput: AnyObject { }

// MARK: - StudentsEvaluationsInput

protocol StudentsEvaluationsInput: AnyObject { }
