//
//  StudentEvaluationsViewModel.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation

// MARK: - StudentEvaluationsViewModelInterface

protocol StudentEvaluationsViewModelInterface {

    var tableData: [StudentsEvaluationTableViewCellModel] { get }

    func viewDidLoad()
}

// MARK: - StudentEvaluationsViewModel

class StudentEvaluationsViewModel {

    weak var view: StudentEvaluationsViewControllerInterface?
    private weak var output: StudentEvaluationsOutput?

    private let student: Student

    var tableData = [StudentsEvaluationTableViewCellModel]()

    init(output: StudentEvaluationsOutput, student: Student) {
        self.output = output
        self.student = student
    }
}

// MARK: - StudentEvaluationsViewModelInterface

extension StudentEvaluationsViewModel: StudentEvaluationsViewModelInterface {

    func viewDidLoad() {
        tableData = student.evaluations.compactMap {
            StudentsEvaluationTableViewCellModel(title: $0.lesson.title, subTitle: "\(round($0.mark))")
        }

        view?.setupTitle("Оценки \(student.lastName) \(student.name)")
    }
}
