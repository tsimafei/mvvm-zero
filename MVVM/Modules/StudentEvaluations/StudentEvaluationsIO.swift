//
//  StudentEvaluationsIO.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation

// MARK: - StudentEvaluationsOutput

protocol StudentEvaluationsOutput: AnyObject { }

// MARK: - StudentEvaluationsInput

protocol StudentEvaluationsInput: AnyObject { }
