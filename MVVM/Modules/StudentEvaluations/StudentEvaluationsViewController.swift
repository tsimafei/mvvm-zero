//
//  StudentEvaluationsViewController.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation
import UIKit

private enum Constants {
    static var studentsEvaluationsCellIdentifier: String { "StudentsEvaluationsTableViewCell" }

    static var backgroundColor: UIColor { .white }
}

// MARK: - StudentEvaluationsViewControllerInterface

protocol StudentEvaluationsViewControllerInterface: AnyObject {
    func setupTitle(_ title: String)
}

// MARK: - StudentEvaluationsViewController

class StudentEvaluationsViewController: UIViewController {

    private let viewModel: StudentEvaluationsViewModelInterface

    let tableView = UITableView()

    init(viewModel: StudentEvaluationsViewModelInterface) {
        self.viewModel = viewModel

        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.viewDidLoad()
        setupTable()

        setupContstraints()
        setupUI()
    }

    private func setupUI() {
        view.backgroundColor = Constants.backgroundColor
    }

    private func setupContstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate(
            [
                tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                tableView.topAnchor.constraint(equalTo: view.topAnchor),
                tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ]
        )
    }

    private func setupTable() {
        view.addSubview(tableView)

        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(
            StudentsEvaluationTableViewCell.self,
            forCellReuseIdentifier: Constants.studentsEvaluationsCellIdentifier
        )
    }
}

// MARK: - StudentEvaluationsViewControllerInterface

extension StudentEvaluationsViewController: StudentEvaluationsViewControllerInterface {
    func setupTitle(_ title: String) {
        self.title = title
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate

extension StudentEvaluationsViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.tableData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(
                withIdentifier: Constants.studentsEvaluationsCellIdentifier,
                for: indexPath
            ) as? StudentsEvaluationTableViewCell
        else { return UITableViewCell() }

        cell.setup(with: viewModel.tableData[indexPath.row])
        return cell
    }
}
