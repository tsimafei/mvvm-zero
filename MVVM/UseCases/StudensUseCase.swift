//
//  StudensUseCase.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation

// MARK: - StudentsUseCase

protocol StudentsUseCase {
    func getAllStudents() -> [Student]
    func getAllLessons() -> [Lesson]
    func getSchedule() -> [(lesson: Lesson, dates: [Date])]
}

// MARK: - StudentsUseCaseImpl

class StudentsUseCaseImpl {
    let mathematic = Lesson(title: "Высшая математика")
    let language = Lesson(title: "Русский язык")
    let geometry = Lesson(title: "Геометрия")

    init() { }
}

// MARK: - StudentsUseCase

extension StudentsUseCaseImpl: StudentsUseCase {

    func getAllLessons() -> [Lesson] {
        [
            mathematic,
            language,
            geometry
        ]
    }

    func getSchedule() -> [(lesson: Lesson, dates: [Date])] {
        return getAllLessons().compactMap {
            return (lesson: $0, dates: (0...8).compactMap { _ in
                var dateComponents = DateComponents()
                dateComponents.day = Int.random(in: 0...30)
                dateComponents.hour = Int.random(in: 8...15)
                dateComponents.minute = Bool.random() ? 0 : 30

                return Calendar.current.date(from: dateComponents)
            })
        }
    }

    func getAllStudents() -> [Student] {
        return [
            Student(
                name: "Линкевич",
                lastName: "Елизавета",
                startDate: Date(),
                evaluations: [
                    Evaluation(lesson: mathematic, mark: Float.random(in: 0...10)),
                    Evaluation(lesson: language, mark: Float.random(in: 0...10)),
                    Evaluation(lesson: geometry, mark: Float.random(in: 0...10))
                ]
            ),
            Student(
                name: "Кислюк",
                lastName: "Максим",
                startDate: Date(),
                evaluations: [
                    Evaluation(lesson: mathematic, mark: Float.random(in: 0...10)),
                    Evaluation(lesson: language, mark: Float.random(in: 0...10)),
                    Evaluation(lesson: geometry, mark: Float.random(in: 0...10))
                ]
            ),
            Student(
                name: "Кречко",
                lastName: "Кирилл",
                startDate: Date(),
                evaluations: [
                    Evaluation(lesson: mathematic, mark: Float.random(in: 0...10)),
                    Evaluation(lesson: language, mark: Float.random(in: 0...10)),
                    Evaluation(lesson: geometry, mark: Float.random(in: 0...10))
                ]
            ),
            Student(
                name: "Пивник",
                lastName: "Николай",
                startDate: Date(),
                evaluations: [
                    Evaluation(lesson: mathematic, mark: Float.random(in: 0...10)),
                    Evaluation(lesson: language, mark: Float.random(in: 0...10)),
                    Evaluation(lesson: geometry, mark: Float.random(in: 0...10))
                ]
            )
        ]
    }
}
