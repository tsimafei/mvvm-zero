//
//  Lesson.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation

struct Lesson {
    let id: String = UUID().uuidString

    let title: String
}
