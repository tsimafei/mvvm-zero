//
//  Evaluation.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation

struct Evaluation {
    let id: String = UUID().uuidString

    let lesson: Lesson
    let mark: Float
}
