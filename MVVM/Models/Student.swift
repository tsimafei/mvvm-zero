//
//  Student.swift
//  MVVM
//
//  Created by Tsimafei Harhun on 13.12.21.
//

import Foundation

struct Student {
    let id: String = UUID().uuidString

    let name: String
    let lastName: String

    let startDate: Date

    let evaluations: [Evaluation]
}
